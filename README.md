# About

This is a presentation touching various high level concepts all related to the common "Clean Code" mantra.

The goal of this presentation is to encourage the audience to not shy away from challenges a legacy codebase brings with it.
It starts with showing some concepts, then continues with showing ways how to solve those challenges (spoiler alert: tooling around metrics, refactorings and testing). At the end it gives some more pointers to related concepts around code quality and extreme programming.

> [!NOTE]
> This presentation doesn't go into detail, instead it gives pointers and remarks on the way to becoming a [Software Craftsman](https://en.wikipedia.org/wiki/Software_craftsmanship).


Topics:
* The "Broken window principle"
* Software Entropy
* Technical debt
* Code smells

## How to use

* check out this repo
* open `index.html` in your browser
* further information about how to use can be found at [reveal.js]

## Fonts

Fonts used are from Google Web Fonts project: http://www.google.com/fonts/# 

<link href='http://fonts.googleapis.com/css?family=Oswald|Merriweather' rel='stylesheet' type='text/css'>


[reveal.js]: https://revealjs.com/